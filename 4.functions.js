
//function is the keyword used to define a function
//functions might receive parameters.
//functions can return values.
//to invoke an function we use the function name followed by ();

// function functionName(parameters) {
//     function body
// }

// named functions ->
function add(num1, num2) {
    return num1 + num2;
}

let addition = add(5, 7);
console.log(addition);

//variable functions ->
const subtract = function (num1, num2) {
    return num1 - num2;
}

let difference = subtract(10, 5);
console.log(difference);

//fat arrow functions ->
//implicit return statement.
let multiply = (num1, num2) => num1 * num2;

//explicit return statement.
let divide = (num1, num2) => {
    return num1/num2;
}

//without return statement ->
let displayMessage = (name) => {
    console.log(`Helloooooo ${name}`);
}

displayMessage("Optimus Prime");
