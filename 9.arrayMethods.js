let toWatch = ["Narcos", "GOT", "Sherlock Holmes", "The Witcher", "Friends", "TBBT"];

let length = toWatch.length;
// 6.

toWatch.push("HIMYM");
// push adds an element at the end of an array.

toWatch.pop();
// pop deletes an element from the end of the array.

toWatch.shift();
// shift deletes an element from the beginning of the array.

toWatch.unshift("Mazya navryachi baiko!");
// unshift adds an element at the start of the array.

let showsWithSmallNames = toWatch.filter((show) => show.length < 6);
console.log(showsWithSmallNames);
// filter method filters the array on a condition(predicate) and returns an new array.

let capitalizedNames = toWatch.map((show) => show.toUpperCase());
console.log(capitalizedNames);
// map method iterates over the array and returns an array with the modifications on every element.