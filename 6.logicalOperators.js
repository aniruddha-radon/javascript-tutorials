// logical operators are used to evaluate the result of 2 or more conditional operators
// OR -> ||

let isStatementTrue = 5 < 6 || 6 > 9;
// evaluates to TRUE because the first condition evaluates to true.

let name = null || "Ruskin Bond";
// will evaluate to "Ruskin Bond" because null is evaluated to FALSE.

// AND -> &&
isStatementTrue = 5 < 6 && 6 > 9;
// evaluates to FALSE because the second condition evaluates to false.

name = null && "Ruskin Bond";
// evaluates to NULL.


// combination
let year = 2020;
let isLeapYear = (year % 4 == 0) && (year % 100 !== 0 || year > 1680);