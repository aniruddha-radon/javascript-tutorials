let number = 10;
let number2 = 12;

// we use the  '+' sign to add.
let addition = number + number2;
console.log(addition);

// we use the  '-' sign to subtract.
let subtraction = number2 - number;
console.log(subtraction);

// we use the  '*' sign to multiply.
let product = number2 * number;
console.log(product);

// we use the  '/' sign to divide.
let division = number2 / number;
console.log(division);


// we use the  '%' sign to get the remainder.
let remainder = number2 % number;
console.log(remainder);



// SHORT HAND ASSIGNMENT

let counter = 0;
counter += 1;
counter -= 1;
counter *= 1;
counter /= 1;

//INCREMENT AND DECREMENT OPERATORS
let noOfStudents = 1;
noOfStudents++;
noOfStudents--;

//EXPONENTIATION
let baseNumber = 2;
let power = 2;

let exponent = baseNumber ** power;