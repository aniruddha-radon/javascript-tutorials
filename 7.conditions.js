// make decisions using 'truthy' or 'falsy' values ->
// if, else if, else.

if(true) {
    console.log("condition evaluated to true");
}

if(false) {
    console.log("condition evaluated to false");
} else {
    console.log("Inside else block because, if condition evaluated to false");
}

if (false) {
    console.log("condition evaluated to false");
} else if (true) {
    console.log("Inside else if block because, if condition evaluated to false");
} else {
    console.log("not coming here because else if condition evaluated to true.");
}

// switch ->
let pokemon = "Charizard";

switch(pokemon) {
    case "Pikachu":
        console.log("Electric Pokemon");
        break;
    case "Charizard":
        console.log("Fire Pokemon");
        break;
    default:
        console.log("Is that even a Pokemon?? :/ ");
}