let string = "A string";

let greeting = "Helloooooo";
let course = "TY BBA CA";

// concatenating strings:

let fullGreeting = greeting + ' ' + course;
console.log(fullGreeting);

// OR with ES6 ->

fullGreeting = `${greeting} ${course}`;
console.log(fullGreeting);
// the ${varName} is called as template literal.

// IMMUTABILITY ->
let name = "Aniruddha";
name[2] = "O";
console.log(name);
// still prints Aniruddha. because strings are immutable in JS.

// ESCAPING ->
// 1. new line = \n
let quote = "Join The Army. Visit exotic places, meet strange people, \nthen kill them.";
console.log(quote);

// 2. tab = \t
quote = "Aniruddha is keeping \t tabs \t on students who do not pay attention.";
console.log(quote);

// 2. print " = \"
quote = "Please don't pay \"attention\", and win a \"TNG\"";
console.log(quote);

// TRYING ARITHMETICS WITH STRING AND NUMBERS ->
let noOfStudents = "10"; //notice that this is a string.
let noOfTeachers = 1;    //notice that this is a number.

let total = noOfStudents + noOfTeachers;
console.log(total);
//the output will be '101' because javascript typecasts (converts) the integer to string when
// trying to add a string and an integer.

// typecasting strings to numbers ->
noOfStudents = parseInt(noOfStudents);
total = noOfStudents + noOfTeachers;
console.log(total);