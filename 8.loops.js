// traditional for loop ->
// for(initialization; condition; statement) {
//      code here..   
// }

for(let i = 0; i < 10; i++) {
    console.log(i);
}

console.log(i); // i is not available because 'let' creates a scoped variable.

// the 'for in' loop ->
let characters = ['Optimus Prime', 'Pikachu', 'Harry Potter', 'Cersei'];

for(let index in characters) {
    let character = characters[index];
    console.log(character);
}

// the 'for of' loop ->
for(let character of characters) {
    console.log(character);
}

// while loop ->
let level = 0;
while(level < 5) {
    console.log("NOOB PLAYAA");
    level += 1;
}

// break and continue ->
for(let i = 0; i < 10; i++) {
    if(i > 5) {
        break;
    }
    console.log(i);
}

for(let i = 0; i < 10; i++) {
    if(i < 5) {
        continue;
    }
    console.log(i);
}