// 

let heightThreshold = 3.5;
let person = {
    height: 2.1,
    age: 22
}

if (heightThreshold < person.height) {
    console.log("allowed inside");
} else if (person.age > 21) {
    console.log("allowed inside because of age");
} else {
    console.log("not allowed inside");
}

