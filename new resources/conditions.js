// compare the left hand side (LHS) to the right hand side (RHS)
// they return a boolean.

let number = 10;
let isEqual = number == 10; // TRUE
let isGreater = number > 7; // TRUE
let isGreaterThanEqualTo = number >= 20; // FALSE
let isLessThan = number < 15; // TRUE
let isLessThanEqualTo = number <= 7; // FALSE
let notEqualTo = number != 15; // TRUE

// triple equal to operator:
let numberAsAString = "10";
isEqual = number == numberAsAString; // TRUE
isEqual = number === numberAsAString;// FALSE

notEqualTo = number != numberAsAString; // FALSE
notEqualTo = number !== numberAsAString; // TRUE