// conventional function
// named function
function subtract(num1, num2) {
    return num1 - num2;
    // return stops the execution of a function
}

subtract(10, 5); // 5


// variable function;

const add = function (num1, num2) {
    return num1 + num2;
}

let addition = add(10, 5);

// FAT ARROW function;

const multiply = (num1, num2) => {
    return num1 * num2;
}

multiply(2, 8);

const printMessage = (message) => {
    console.log(message);
}

printMessage("Hi TY BBA CA, long time no see!")