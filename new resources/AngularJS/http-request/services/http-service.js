app.service("dataService", function($http) {
    this.getPosts = () => {
        return $http.get("https://jsonplaceholder.typicode.com/posts");
    }
});