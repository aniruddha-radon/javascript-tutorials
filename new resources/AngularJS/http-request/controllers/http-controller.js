app.controller(
    "httpController", 


    function ($scope, dataService) {
    $scope.posts = [];

    $scope.getPosts = () => {
        // returns an promise(for asynchronous calls).
        dataService.getPosts().then(function (response) {
            $scope.posts = response.data;
        })

    }

    $scope.getPosts();
}


);