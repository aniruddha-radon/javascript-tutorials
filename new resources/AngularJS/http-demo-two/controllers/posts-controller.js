app.controller("postsController", function($scope, dataService) {
    $scope.posts = [];

    $scope.getData = () => {
        dataService.getPosts().then(function(response) {
            $scope.posts = response.data;
        })
    }

    $scope.getData();
})