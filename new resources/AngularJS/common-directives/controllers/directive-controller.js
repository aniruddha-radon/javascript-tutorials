app.controller("directiveController", function ($scope) {

    $scope.people = [

        {
            name: "Albert Einstein",
            profession: "physicists",
            isAlive: false
        },

        {
            name: "Richard Feynman",
            profession: "physicists",
            isAlive: false
        },

        {
            name: "K Sivan",
            profession: "physicists",
            isAlive: true
        },

        {
            name: "Marie Curie",
            profession: "Chemist",
            isAlive: false
        }];

    $scope.person = '';
    $scope.profession = '';

    $scope.toggleAlive = (index) => {
        $scope.people[index].isAlive = !$scope.people[index].isAlive;
    }

    $scope.addPerson = () => {
        const newPerson = {
            name: $scope.person,
            profession: $scope.profession,
            isAlive: true
        }
        $scope.people.push(newPerson);
    }



    $scope.customStyle = {
        "background-color": "teal",
        "height": "300px",
        "width": "300px",
        "border": "thick solid red"
    };

    $scope.changeDivStyle = () => {
        $scope.customStyle = {
            "background-color": "blue",
            "height": "200px",
            "width": "200px",
            "border": "20px solid yellow"
        }
    }

    $scope.showNgClassDiv = true;

    $scope.toggleNgClassDiv = () => {
        $scope.showNgClassDiv = !$scope.showNgClassDiv;
    }

})