// 3 types of loops

// conventional

// for(initialization; condition; statement) {

// }

var series = ["GOT", "TBBT", "HIMYM"];

for(let i = 0; i < 10; i++) {
    console.log(i);
}

for (let i = 0; i < series.length; i++) {
    console.log(series[i]);
}


// for in loop.  ALWAYS USED WITH ARRAYS.

for(let index in series) {
    console.log(series[index]);
}


// for OF loop. ALWAYS USED WITH ARRAYS.

for(let serial of series) {
    console.log(serial);
}

// continue and break.
let OSs = ["windows", "Linux", "RedHat", "Fedora", "MacOS"];

for(let os of OSs) {
    console.log(os);
    if(os === "RedHat") {
        console.log("RedHat Found");
        break;
    }
};

let numbersTwo = [1,2,3,4,5,6];

// skip every number which is divisible by 5.

for(let number of numbersTwo) {
    if(number % 5 === 0) {
        continue;
    }
    console.log(number);
}