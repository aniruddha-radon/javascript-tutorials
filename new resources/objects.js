let student = {
    name: "Ram",
    degree: "TY BBA CA",
    previousEducation: [
        {
            degree: "HSC",
            percentage: 70
        },
        {
            degree: "SSC",
            percentage: 40
        }
    ]
};

student["previousEducation"][1]["percentage"]