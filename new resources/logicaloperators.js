let number = 15;
let numberTwo = 20;

// OR -> || 
// AND -> &&

let age = 18;

let isInBetween = (number < age) && (age < numberTwo); // TRUE

let lowerLimit = 18;
let upperLimit = 30;

let isConditionMet = (lowerLimit < age) || (age < upperLimit);