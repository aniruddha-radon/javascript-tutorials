let stringVariable = "this is a string";
let stringVariableTwo = 
    'this is a single quoted string';
let backTickString = `a back ticked string`;


// CONCATENTATION (JOINING) '+'
let name = "Aniruddha";
let education = 'MCA';

let finalString = name + education;

// template literals (interpolation)
finalString = `${name} is ${education}`;


let noOfApples = 10;
let noOfOranges = "25";

let countOfFruits = noOfApples + noOfOranges;

countOfFruits = noOfApples + parseInt(noOfOranges);