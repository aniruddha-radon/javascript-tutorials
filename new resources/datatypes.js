// let and var;
let integerVariable = 10;
let decimalVariable = 5.7;
let stringVariable = "this is a string";
let stringVariableTwo = 'this is a single quoted string';
let backTickString = `a back ticked string`;
let booleanVariable = true;
let booleanVariableTwo = false;


let undefinedVariable = undefined;
let dummyVariable;
// this variable was NEVER assigned a value.


let nullVariable = null;
// there was value assigned but now we dont want it.


let arrayVariable = [
    1, // 0
    "string", // 1 
    false, // 2
    undefined, // 3
    null, // 4
    22.3,  // 5
    [1,2,3,"a string"] // 6
];
// Not HOMOGENOUS.

let objectVariable = {
    key: 10,
    name: "Aniruddha",
    marks: [12, 13, 14]
};

objectVariable.name