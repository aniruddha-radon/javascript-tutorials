// let and var

var myVariable;
let myVariableTwo;

myVariable = 5; // = assignment operator and '5' is called the literal
myVariable = 7;

// const
const RADIUSOFCIRCLE = 25;
RADIUSOFCIRCLE = 30; // INVALID;


