// callbacks.

const add = (num1, num2) => {
    return num1 + num2;
}


const callBackExample = (functionAsParameter) => {
    functionAsParameter("Riya");
}

const printMessage = (name) => {
    console.log(`Hi ${name}`);
}

callBackExample(printMessage);


const map = (array, callback) => {
    const newArray = [];
    for(let item of array){
        let newItem = callback(item);
        newArray.push(newItem);
    }
    return newArray;
}

let array = [1, 2, 3, 4, 5];
const square = (num) => {
    return num ** 2;
}

// array = map(array, square);

array = array.map(square);

const odd = (num) => {
    return num % 2 !== 0;
} 

array = array.filter(odd);
