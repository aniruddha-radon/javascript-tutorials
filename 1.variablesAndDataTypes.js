// we can declare variables using 3 keywords - var, let and const;

var myVar;

let myVariable;

const PI = 3.14;
//constants must always be declared with a value. (declaring + defining)
//all variables which have been declared but not defined have the value 'undefined';
//any variable which is not declared is 'not defined'.

let integer = 10;
let decimal = 10.5;
let string = "strings are quoted characters!!";
let boolean = true; // or false;
let undefinedVar = undefined;
let nullVar = null;
let array = [];
let object = {};

// even though the above variables are 'datatyped',
// JavaScript does not assign them a STRONG type.