// some expression evaluate to either true or false ->

let isEqual = 1 == 1;
let isLessThan = 1 < 2;
let isLessThanOrEqualTo = 5 <= 3;
let isGreaterThan = 1 > 2;
let isGreaterThanEqualTo = 5 >= 3;
let isNotEqualTo = 5 != 3;

// JavaScripts typechecker -> extra equal to:

let equality = 1 == "1";
// will evaluate to true because == checks only the value not the data type.

equality = 1 === "1";
// will evaluate to false because === checks both value and the data type.
let inequality = 1 !== "1";
// will evaluate to true because the number 1 is not equal to the string "1".

// COMMON PITFALLS ->
let isArrayEqual = [] === [];
let isObjectEqual= {} === {};